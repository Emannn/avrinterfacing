/*
 * dio.h
 *
 * Created: 27/09/2019 17:57:02
 *  Author: Eman
 */ 


#ifndef DIO_H_
#define DIO_H_


#include "dio_types.h"
#include "std_types.h"


/* functions prototype*/

void Dio_WriteChannel(Dio_ChannelType Channel, STD_levelType level);
void DIO_Read(Dio_ChannelType channel,uint8 *data);
void Dio_FlipChannel( Dio_ChannelType ChannelId);
STD_levelType Dio_ReadChannel(Dio_ChannelType ChannelId); //TODO




#endif /* DIO_H_ */