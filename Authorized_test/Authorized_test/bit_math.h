/*
 * bit_math.h
 *
 * Created: 27/09/2019 01:30:30
 *  Author: Eman
 */ 


#ifndef BIT_MATH_H_
#define BIT_MATH_H_




#define SET_BIT(Reg,bit)    Reg|=(1<<bit)
#define CLR_BIT(Reg,bit)    Reg&=~(1<<bit)
#define TOGGLE_BIT(Reg,bit) Reg^=(1<<bit)
#define GET_BIT(Reg,bit)   ((Reg>>bit) & 0x01)



#endif /* BIT_MATH_H_ */