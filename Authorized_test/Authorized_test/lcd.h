/*
 * lcd.h
 *
 * Created: 27/09/2019 01:20:21
 *  Author: Eman
 */ 


#ifndef LCD_H_
#define LCD_H_


#include "dio_reg.h"


#define  LCD_Dir DDRA     //data direction for lcd pins
#define  LCD_Port PORTA    //data PORTA for lcd
#define  LCD_CONT_DIR DDRB
#define  LCD_Control PORTB

#define RS 1
#define RW 2
#define E  3

// function prototype

void LCD_Init(void);
void LCD_Char(unsigned char data );
void LCD_Command(unsigned char cmnd );
void LCD_Clear();
void LCD_String();





#endif /* LCD_H_ */