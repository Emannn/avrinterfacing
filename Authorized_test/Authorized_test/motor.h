/*
 * motor.h
 *
 * Created: 27/09/2019 18:54:58
 *  Author: Eman
 */ 


#ifndef MOTOR_H_
#define MOTOR_H_


//prototype

void motor_cw(void);
void motor_ccw(void);
void motor_stop(void);
void motor_right(void);
void motor_left(void);

#endif /* MOTOR_H_ */