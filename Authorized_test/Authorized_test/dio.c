/*
 * dio.c
 *
 * Created: 27/09/2019 17:56:51
 *  Author: Eman
 */ 
 #include "dio.h"
 #include "dio_reg.h"
 #include "dio_types.h"
 #include "std_types.h"
 #include "bit_math.h"

 

 void Dio_WriteChannel(Dio_ChannelType channel,STD_levelType level){
	 Dio_PortType channel_port=channel/8;
	 Dio_ChannelType ChannelPos =channel%8;
	 switch(channel_port){
		 case Dio_Port_A:
		 if(level == STD_low)
		 {
			 CLR_BIT(PORTA,ChannelPos );
		 }
		 else
		 {
			 SET_BIT(PORTA,ChannelPos );
		 }
		 break;
		 case Dio_Port_B:
		 if(level == STD_low)
		 {
			 CLR_BIT(PORTB,ChannelPos );
		 }
		 else
		 {
			 SET_BIT(PORTB,ChannelPos );
		 }
		 break;
		 case Dio_Port_C:
		 if(level == STD_low)
		 {
			 CLR_BIT(PORTC,ChannelPos );
		 }
		 else
		 {
			 SET_BIT(PORTC,ChannelPos );
		 }
		 break;
		 case Dio_Port_D:
		 if(level == STD_low)
		 {
			 CLR_BIT(PORTD,ChannelPos );
		 }
		 else
		 {
			 SET_BIT(PORTD,ChannelPos );
		 }
		 break;
		 
	 }
	 
 }
 /*
 *Read channel
 */
 void DIO_Read(Dio_ChannelType channel,uint8 *data){
	 Dio_PortType channel_port=channel/8;
	 Dio_ChannelType channelPos=channel%8;
	 switch(channel_port){
		 case Dio_Port_A:
		 *data=GET_BIT(PINA,channelPos);
		 break;
		 
		 case Dio_Port_B:
		 *data=GET_BIT(PINB,channelPos);
		 break;
		 case Dio_Port_C:
		 *data=GET_BIT(PINC,channelPos);
		 break;
		 
		 case Dio_Port_D:
		 *data=GET_BIT(PIND,channelPos);
		 break;
		 
	 }
	 
 }
