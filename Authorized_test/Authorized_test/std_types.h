/*
 * std_types.h
 *
 * Created: 27/09/2019 18:04:30
 *  Author: Eman
 */ 


#ifndef STD_TYPES_H_
#define STD_TYPES_H_

typedef unsigned char uint8;
typedef unsigned long long  uint32;

typedef signed char int8;
typedef signed long long  int32;




#endif /* STD_TYPES_H_ */