/*
 * lcd.c
 *
 * Created: 27/09/2019 01:20:35
 *  Author: Eman
 */ 
 #define F_CPU   16000000

 #include "LCD.h"
 #include <util/delay.h>

 void LCD_Init (void)  /* LCD Initialize function */
 {
	 LCD_Dir = 0xFF;		/* DDRA=0X111 Make LCD port direction as o/p */
	 LCD_CONT_DIR=0x0F; /*  DDRB=0X01 Make LCD port direction as o/p */
	 LCD_Control &= ~(1<<2);
	 _delay_ms(20);		/* LCD Power ON delay always >15ms */
	 


	 LCD_Command(0x33);
	 LCD_Command(0x32);	/* Send for 4 bit initialization of LCD  */
	 LCD_Command(0x28);	/* 2 line, 5*7 matrix in 4-bit mode */
	 LCD_Command(0x0c);	/* Display on cursor off */
	 LCD_Command(0x06);	/* Increment cursor (shift cursor to right) */
	 LCD_Command(0x01);	/* Clear display screen */


 }

 void LCD_Char( unsigned char data )
 {
	 LCD_Port = (LCD_Port & 0x0F) | (data & 0xF0);/* Sending upper nibble */
	 LCD_Control |= (1<<RS);  /* RS=1, data reg. */
	 LCD_Control|= (1<<E);
	 _delay_us(10);
	 LCD_Control &= ~ (1<<E);
	 _delay_us(200);
	 LCD_Port = (LCD_Port & 0x0F) | (data << 4);  /* Sending lower nibble */
	 LCD_Control |= (1<<E);
	 _delay_us(10);
	 LCD_Control &= ~ (1<<E);
	 _delay_ms(7);
 }

 void LCD_Command( unsigned char cmnd )
 {
	 LCD_Port = (LCD_Port & 0x0F) | (cmnd & 0xF0);/* Sending upper nibble */
	 LCD_Control &= ~ (1<<RS);		/* RS=0, command reg. */
	 LCD_Control |= (1<<E);		/* Enable pulse */
	 _delay_us(10);
	 LCD_Control &= ~ (1<<E);
	 _delay_us(2000);
	 LCD_Port = (LCD_Port & 0x0F) | (cmnd << 4);/* Sending lower nibble */
	 LCD_Control |= (1<<E);
	 _delay_us(1);
	 LCD_Control &= ~ (1<<E);
	 _delay_ms(2);
 }
 void LCD_String (char *str)		/* Send string to LCD function */
 {
	 int i;
	 for(i=0;str[i]!=0;i++)		/* Send each char of string till the NULL */
	 {
		 LCD_Char (str[i]);
	 }
 }
void LCD_Clear()
{
	LCD_Command (0x01);		/* Clear display */
	_delay_ms(2);
	LCD_Command (0x80);		/* Cursor at home position */
}


