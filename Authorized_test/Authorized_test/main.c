/*
 * Authorized_test.c
 *
 * Created: 27/09/2019 01:17:11
 * Author : Eman
 */ 

 #define F_CPU   16000000


#include <util/delay.h>
#include "dio_reg.h"
#include "lcd.h"
#include "bit_math.h"
#include "motor.h"


#define buzzer 3   //PA3
#define  btn1 4    //PB4
#define  btn2 2    //PD2
#define  btn0 0   //PB0
#define  led0 2   //PC2

int main(void)
{

//cfgr buzzer as output
SET_BIT(DDRA,buzzer);
//cfgr btn1 as input
CLR_BIT(DDRB,btn1);
//cfgr btn2 as input
CLR_BIT(DDRD,btn2);
//cfgr btn0 as input
CLR_BIT(DDRB,btn0);





	LCD_Init();

    /* Replace with your application code */
     while(1){

	   if (GET_BIT(PINB,btn1)==0x01)       //btn1  /*authorized & off_bzr & motor_cw on & motor_stop */
	   {
            LCD_Clear();
	        LCD_String("Authorized & cw");
	       	_delay_ms(1000);
		    motor_cw();

		    CLR_BIT(PORTA,buzzer);
	   } 
	   else if (GET_BIT(PIND,btn2)==0x01)    //btn2  /*non-authorized & on_bzr & motor_stop */
	   {
	       LCD_Clear();
	   	   LCD_String("Non-Authorized");

		  SET_BIT(PORTA,buzzer);

		  	_delay_ms(1000);

			CLR_BIT(PORTA,buzzer);

			motor_stop();

	   }
	   else if (GET_BIT(PINB,btn0)==0x01)    //btn0    /*stop buzzer & motor_ccw */ 
	   {			

	     LCD_Clear();
	     _delay_ms(50);
	   //  CLR_BIT(PORTA,buzzer);
	    LCD_String("Authorized & ccw");
		 motor_ccw();
	   }
	   
	}

    
}

