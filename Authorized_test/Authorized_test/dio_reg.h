/*
 * dio_reg.h
 *
 * Created: 27/09/2019 17:58:25
 *  Author: Eman
 */ 


#ifndef DIO_REG_H_
#define DIO_REG_H_

 #include "std_types.h"


// Register A
#define PORTA	*((volatile uint8*)0x3B)
#define DDRA	*((volatile uint8*)0x3A)
#define PINA	*((volatile uint8*)0x39)

// Register B
#define PORTB	*((volatile uint8*)0x38)
#define DDRB	*((volatile uint8*)0x37)
#define PINB	*((volatile uint8*)0x36)

// Register C
#define PORTC	*((volatile uint8*)0x35)
#define DDRC	*((volatile uint8*)0x34)
#define PINC	*((volatile uint8*)0x33)


// Register D
#define PORTD	*((volatile uint8*)0x32)
#define DDRD	*((volatile uint8*)0x31)
#define PIND	*((volatile uint8*)0x30)



#endif /* DIO_REG_H_ */