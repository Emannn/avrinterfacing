/*
 * motor.c
 *
 * Created: 27/09/2019 18:54:48
 *  Author: Eman
 */ 
 #include "dio.h"

 void motor_cw(void)
 {
 //A1
 Dio_WriteChannel(Dio_Channel_C3,STD_high);
 //A2
 Dio_WriteChannel(Dio_Channel_C4,STD_low);
 //EN1
 Dio_WriteChannel(Dio_Channel_D4,STD_high);

 }
 void motor_ccw(void)
 {/*motor 1 */
	 //A1
	 Dio_WriteChannel(Dio_Channel_C3,STD_low);
	 //A2
	 Dio_WriteChannel(Dio_Channel_C4,STD_high);
	 //EN1
	 Dio_WriteChannel(Dio_Channel_D4,STD_high);

 }
 void motor_stop(void)
 {
 /*motor 1*/
  //A1
  Dio_WriteChannel(Dio_Channel_C3,STD_low);
  //A2
  Dio_WriteChannel(Dio_Channel_C4,STD_low);
  //EN1
  Dio_WriteChannel(Dio_Channel_D4,STD_low);

 }
 void motor_right()
 {
 /*motor1*/

 //A1
 Dio_WriteChannel(Dio_Channel_C3,STD_low);
 //A2
 Dio_WriteChannel(Dio_Channel_C4,STD_low);
 //EN1
 Dio_WriteChannel(Dio_Channel_D4,STD_low);

  /*motor2*/

 //A3
 Dio_WriteChannel(Dio_Channel_C5,STD_high);
 //A4
 Dio_WriteChannel(Dio_Channel_C6,STD_low);
 //EN2
 Dio_WriteChannel(Dio_Channel_D5,STD_high);
 


 }
 void motor_left()
{ 

/*motor1*/

 //A1
 Dio_WriteChannel(Dio_Channel_C3,STD_high);
 //A2
 Dio_WriteChannel(Dio_Channel_C4,STD_low);
 //EN1
 Dio_WriteChannel(Dio_Channel_D4,STD_high);

 /*motor2*/

 //A3
 Dio_WriteChannel(Dio_Channel_C5,STD_low);
 //A4
 Dio_WriteChannel(Dio_Channel_C6,STD_low);
 //EN2
 Dio_WriteChannel(Dio_Channel_D5,STD_low);
 
}